package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSimple<T> primero;
	private NodoSimple<T> ultimo;
	private NodoSimple<T> actual;
	private int tamLista;

	public ListaEncadenada(){
		tamLista = 0;
		primero = null;
		ultimo = primero;
		actual = primero;
	}
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			NodoSimple<T> nuevo = primero;

			public boolean hasNext() {
				if(primero == null)
					return false;
				if(nuevo.darSiguiente()== null)
					return false;
				else
					return true;	
			}
			public T next() {
				T elem = null;
				if(nuevo != null){
					elem = nuevo.darItem();
					nuevo=nuevo.darSiguiente();
				}
				return elem;
			}
			
			public void remove() {
				// TODO Auto-generated method stub
				
			}

		};
	}


	public void agregarElementoFinal(T elem) {
		NodoSimple<T> act = new NodoSimple<T>(elem);
		if(primero == null){
			primero = act;
			actual = primero;
			ultimo = primero;
		}else{
			ultimo = darUltimo();
			ultimo.cambiarSiguiente(act);
		}
		tamLista++;

	}

	public NodoSimple<T> darUltimo(){
		NodoSimple<T> actual = primero;
		while(actual.darSiguiente()!= null)
		{
			actual = actual.darSiguiente();
		}
		return actual;
	}

	public T darElemento(int pos) {
		T resp=null;
		int contador=0;
		NodoSimple<T> actual= primero;
		while(actual!=null)
		{
			T ob= actual.darItem();
			if (contador==pos) {
				resp=ob;
				break;
			}
			actual=actual.darSiguiente();
			contador++;
		}
		return resp;
	}


	
	public int darNumeroElementos() {
		return tamLista;
	}
	public T darElementoPosicionActual() {
		return actual.darItem();
	}


	public boolean avanzarSiguientePosicion() {
		NodoSimple<T> sig = actual;
		if(sig == null)
			return false;
		else
			actual = sig.darSiguiente();
		return true;
	}


	public boolean retrocederPosicionAnterior() {
		NodoSimple<T> ant = actual;
		if(primero.darSiguiente() == null)
			return false;
		if((ant.darSiguiente()).darSiguiente()!= null)
			ant = ant.darSiguiente();
		return true;
	}
	public void borrarElementos()
	{
		if(primero.darSiguiente()!=null)
		{
        primero.cambiarSiguiente(null);
        primero = null;
		}
		else 
			primero = null;
		
        tamLista=0;
	}
	public void cambiarPrimero(NodoSimple<T> nodo){
		primero = nodo;
	}
	public NodoSimple<T> darPrimero(){
		return primero;
	}
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}
}