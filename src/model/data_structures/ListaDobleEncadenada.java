package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> cabeza;
	private NodoDoble<T> actual;
	private int tamLista;

	public ListaDobleEncadenada()
	{
		cabeza= null;
		actual=cabeza;
		tamLista = 0;
	}
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>()
				{

			private NodoDoble<T> temp = cabeza;

			public boolean hasNext() {
				return temp != null;

			}

			public T next() {

				T resultado = null;
				if(temp != null)
				{
					resultado = temp.darActual();
					temp = temp.darSiguiente();
				}

				return resultado;
			}

			public void remove() {
				// TODO Auto-generated method stub

			}
				};
	}

	public T eliminarElemento(){
		// ultimo es el primero en la pila
		NodoDoble<T> last = darUltimo();
		T item = last.darActual();
		(last.darAnterior()).cambiarSiguiente(null);
		return item;
	}
	public void agregarElementoFinal(T elem) {
		NodoDoble<T> nuevo = new NodoDoble<T>(elem);
		if(cabeza == null){
			cabeza = nuevo;
			actual = cabeza;
			tamLista++;
		}
		else{
			NodoDoble<T> aux = cabeza;
			while(aux.darSiguiente() != null){
				aux = aux.darSiguiente();

			}
			aux.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(aux);
			tamLista++;
		}
	}
	public T darElemento(int pos) {

		T resp=null;
		int contador=0;
		NodoDoble<T> actual= cabeza;
		while(actual!=null)
		{
			T ob= actual.darActual();
			if (contador==pos) {
				resp=ob;
				break;
			}
			actual=actual.darSiguiente();
			contador++;
		}
		return resp;
	}


	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tamLista;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darActual();
	}

	public boolean avanzarSiguientePosicion() {
		NodoDoble<T> temp = actual;
		if(temp==null)
			return false;
		else
			actual = temp.darSiguiente();
		return true;

	}

	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		NodoDoble<T> temp = actual;
		if(temp==null)
			return false;
		else
			actual = temp.darAnterior();
		return true;
	}
	public NodoDoble<T> darUltimo(){
		NodoDoble<T> act = cabeza;
		while( act.darSiguiente() != null){
			act = act.darSiguiente();
		}
		return act;
	}
	public void ordenar() {
		
	}
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

}
