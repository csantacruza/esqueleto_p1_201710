package model.data_structures;

public class NodoSimple<T>{

	private NodoSimple<T> siguiente;
	private T item;

	public NodoSimple( T elem){
		item = elem;
		siguiente = null;
	}

	public T darItem(){
		return item;
	}
	public NodoSimple<T> darSiguiente(){
		return siguiente;
	}
	public void cambiarSiguiente(NodoSimple<T> nodo){
		siguiente = nodo;
	}
}
