package model.data_structures;

public class Stack<T>{
	
	private ListaDobleEncadenada<T> listaDoble = new ListaDobleEncadenada<T>();
	
	public boolean isEmpty()
	{
		return listaDoble.darNumeroElementos() == 0;
	}
	public int size()
	{
		return listaDoble.darNumeroElementos();
	}
	public void push(T item)
	{
		listaDoble.agregarElementoFinal(item);
	}
	public T pop() 
	{
		return listaDoble.eliminarElemento();
	}
	public void ordenar() {
		listaDoble.ordenar();
	}
	
	
}
